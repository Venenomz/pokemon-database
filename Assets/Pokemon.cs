﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;

public class Pokemon : MonoBehaviour
{
    public GameObject cube;

    public Color[] colours;

    float x = 0;

    float weightOffset = 2.0f;

    float baseWeightScale = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        CreateDataBase();
    }

    void CreateDataBase()
    {
        // set the path
        string path = Application.dataPath + "\\veekun-pokedex.sqlite";

        // Connect to sqlite using the path
        SqliteConnection dbconn =
        new SqliteConnection("Data Source = \"" + path + "\"; Version = 3; ");

        // Open the connection
        dbconn.Open();

        // Create a command that selects all from the pokemon
        SqliteCommand command =
        new SqliteCommand("select * from pokemon, pokemon_species where pokemon.species_id = pokemon_species.id", dbconn);

        // Read one line from the database
        SqliteDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
            var c = colours[(int)(long)reader["color_ID"] - 1];

            float weight = (float)(long)reader["weight"];
            float height = (float)(long)reader["height"];

            var created = Instantiate(cube, new Vector3(x, 0, 0), Quaternion.identity);

            created.GetComponent<MeshRenderer>().material.color = c;

            float weightScale = 500.0f;

            Vector3 scale = new Vector3(weight / weightScale, height / 10.0f, weight / weightScale);

            created.transform.localScale = scale;

            created.gameObject.name = (string)reader["identifier"];

            x += (weight / weightOffset) / weightScale + baseWeightScale;

            created.transform.position = new Vector3(x, 0, 0);

            x += (weight / weightOffset) / weightScale + baseWeightScale;
        }
    }
}
